'use strict';

const config = require('./config/database');
const knex = require('knex')(config);

const vitamin = require('vitamin')(knex);


module.exports = vitamin;
