'use strict';
const Vitamin = require('./');

module.exports = Vitamin.model('User', {
  tableName: 'users',

  hasTimestamps: [
     'created_at',
     'updated_at'
  ],
});
