'use strict';
const Vitamin = require('./');

module.exports = Vitamin.model('Client', {
  tableName: 'clients',

  hasTimestamps: [
     'created_at',
     'updated_at'
  ],
});
