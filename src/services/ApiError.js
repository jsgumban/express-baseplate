'use strict';

class ApiError extends Error {
   constructor({name, message, statusCode, errors}) {
     super(message || 'Error');
     this.name = name || 'Error';
     this.status = statusCode || 403;
     this.errors = errors || [];
     this.fail = true;
  }

  toJSON() {
    return _.pick(this, ['status', 'name', 'message', 'errors', 'fail']);
  }
}

module.exports = ApiError;
