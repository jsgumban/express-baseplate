const { Resource, ResourceCollection } = require('@senhung/http-resource');

class UserResource extends Resource {
   toJson(resource) {
      resource = resource.data;
      return {
         id: resource.id,
         email: resource.email,
         first_name: resource.first_name,
         last_name: resource.last_name
      }
   }
}

module.exports = {
   make:       (resource)  => new UserResource(resource),
   collection: (resources) => new ResourceCollection(resources, UserResource),
};
