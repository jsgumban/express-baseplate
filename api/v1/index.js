const router = require('express').Router();
const auth = require('./auth');
const authorizedUser = require('./middlewares/authorized-user');
const authorizedClient = require('./middlewares/authorized-client');

router.use('/auth', authorizedClient, auth);

module.exports = router;
